<p  align="center">
<a>
<img  src="https://res.cloudinary.com/server-me/image/upload/v1634048615/images/avatar_global_n5vz8e.png"  width="320"  alt="Server API Logo"  />
</a>
</p>
<p  align="center">A Source Server API built in Nest JS for learning  and development.</p>



## Description

  - Built in [Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.
  - Database using [My SQL](https://www.mysql.com/) and [TypeORM](https://typeorm.io/#/).
  - Source runing docker. Learn [docker](https://docs.docker.com/).
  - Build docs api with [swagger](https://swagger.io/).
  - Comming soon!

## Feature
- Authentication :
	+ ✅ Register user
	+ ✅ Login.
	+ ✅ Profile.
	+ ✅ Change password.
	+ ✅ Update profile.
	+ Fogot password. [Comming soom]
	+ Reset password. [Comming soom]
	+ Remove account. [Comming soom]
- Todos example:
	+ ✅ Created.
	+ ✅ Updated.
	+ ✅ Detele.
	+ ✅ Get.
	+ ✅ Filter status.
- Notifications :
	+ Push notification Web - App. [Comming soom]
	+ Archive notifications. [Comming soom]
- Web socket ( Chat realtime )
	+ Open local server chat.  [Comming soom]
	+ Chat group and chat one ( web - app ) : [Comming soom].

## Installation


```bash

npm install or yarn or yarn install

```



## Running the app



```bash

# development

$ npm run start



# watch mode

$ npm run start:dev



# production mode

$ npm run start:prod

```



## Test



```bash

# unit tests

$ npm run test



# e2e tests

$ npm run test:e2e



# test coverage

$ npm run test:cov

```



## Stay in touch



- Author - [Peanut201](https://www.facebook.com/truongdq2001/)

- Website - [Portfolio](https://portfolio-peanut.netlify.app/)

- Twitter - [@Peanut201](https://twitter.com/truong20013)

- Telegram - [@Peanut201](https://t.me/peanut201)



## License


Copyrighted by Peanut201.