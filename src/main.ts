import { ValidationPipe, VersioningType } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as cookieParser from 'cookie-parser';
import * as compression from 'compression';
import * as helmet from 'helmet';
import * as express from 'express';
import { NestExpressApplication } from '@nestjs/platform-express';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { join } from 'path';

// enable HMR hot module

declare const module: any;

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {});
  app.use(helmet());
  app.enableVersioning({
    type: VersioningType.URI,
  });

  app.use(cookieParser());
  app.use(compression());
  app.use('/static', express.static(join(__dirname, '..', 'static')));
  // app.use(csurf());

  //start binding validationPipe at the application level. thus ensuring all endpoints are protected from receiving incorrect data.
  app.useGlobalPipes(
    new ValidationPipe({
      // automatically removed properties from the resulting DTO
      whitelist: true,

      // return exception and stop request when non-whitelisted properties are present.
      forbidNonWhitelisted: true,

      // automatically transform payload to be object typed according to their DTO classes.
      transform: true,
    }),
  );

  // config document swagger
  const config = new DocumentBuilder()
    .setTitle('Server Global')
    .setDescription('The Open API for developer and learning backend!')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  const configService = app.get(ConfigService);
  const port = configService.get<number>('PORT');

  await app.listen(port);

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
}
bootstrap();
