export enum TodoStatus {
  NOT_INIT = 'NOT_INIT',
  TODO = 'TODO',
  DOING = 'DOING',
  VERIFY = 'VERIFY',
  DONE = 'DONE',
  CANCEL = 'CANCEL',
  PENDING = 'PENDING',
  TRASH = 'TRASH',
}
