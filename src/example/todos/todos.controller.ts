import { FilterTodoDto } from './dto/filter-todo.dto';
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  ParseIntPipe,
  Query,
} from '@nestjs/common';
import { TodosService } from './todos.service';
import { CreateTodoDto } from './dto/create-todo.dto';
import { UpdateTodoDto } from './dto/update-todo.dto';
import { ApiProperty, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ResponseListTodoDto, ResponseTodoDto } from './dto/response-todo.dto';

@Controller({
  path: 'todos',
  version: '1',
})
export class TodosController {
  constructor(private readonly todosService: TodosService) {}

  @ApiTags('Example')
  @ApiProperty({ type: CreateTodoDto })
  @ApiResponse({ type: ResponseTodoDto })
  @Post()
  create(@Body() createTodoDto: CreateTodoDto) {
    return this.todosService.create(createTodoDto);
  }

  @ApiTags('Example')
  @ApiResponse({
    type: ResponseListTodoDto,
  })
  @Get()
  findAll() {
    return this.todosService.findAll();
  }

  @ApiTags('Example')
  @ApiProperty({ type: FilterTodoDto })
  @ApiResponse({
    type: ResponseListTodoDto,
  })
  @Post('filter')
  filter(@Query() filterTodoDto: FilterTodoDto) {
    return this.todosService.filter(filterTodoDto);
  }

  @ApiTags('Example')
  @ApiProperty({ type: { id: Number } })
  @ApiResponse({ type: ResponseTodoDto })
  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.todosService.findOne(id);
  }

  @ApiTags('Example')
  @ApiProperty({ type: UpdateTodoDto })
  @ApiResponse({ type: ResponseTodoDto })
  @Patch()
  update(@Body() updateTodoDto: UpdateTodoDto) {
    return this.todosService.update(updateTodoDto);
  }
}
