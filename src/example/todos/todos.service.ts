import { BaseResponse } from '@base/types';
import { FilterTodoDto } from './dto/filter-todo.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { CreateTodoDto } from './dto/create-todo.dto';
import { UpdateTodoDto } from './dto/update-todo.dto';
import { Todos } from './entities/todo.entity';
import { Repository } from 'typeorm';
import { CommonResult } from '@base/exception';

@Injectable()
export class TodosService {
  private readonly logger = new Logger(TodosService.name);
  constructor(
    @InjectRepository(Todos) private todoRepository: Repository<Todos>,
  ) {}

  async create({
    content,
    status,
  }: CreateTodoDto): Promise<BaseResponse<Todos>> {
    try {
      const newTodo = this.todoRepository.create({
        content: content,
        status: status,
      });
      const todoCreated = await newTodo.save();
      return CommonResult<Todos>(todoCreated, HttpStatus.OK, 'Todo Created');
    } catch (error) {
      return CommonResult<null>(
        null,
        HttpStatus.INTERNAL_SERVER_ERROR,
        'Oops! Something went wrong!',
        error,
      );
    }
  }

  async findAll(): Promise<BaseResponse<Todos[]>> {
    try {
      const listTodo = await this.todoRepository.find();
      return CommonResult<Todos[]>(listTodo, HttpStatus.OK, 'Successfully!');
    } catch (error) {
      return CommonResult<null>(
        null,
        HttpStatus.INTERNAL_SERVER_ERROR,
        'Oops! Something went wrong!',
        error,
      );
    }
  }

  async filter({ status }: FilterTodoDto): Promise<BaseResponse<Todos[]>> {
    try {
      const listMatchTodo = await await this.todoRepository.find({
        where: { status },
      });
      return CommonResult<Todos[]>(
        listMatchTodo,
        HttpStatus.OK,
        'Filter todo successfully!',
      );
    } catch (error) {
      return CommonResult<null>(
        null,
        HttpStatus.INTERNAL_SERVER_ERROR,
        'Oops! Something went wrong!',
        error,
      );
    }
  }

  async findOne(id: number): Promise<BaseResponse<Todos>> {
    try {
      const todoExisting = await this.todoRepository.findOne({ id });

      if (todoExisting) {
        return CommonResult<Todos>(
          todoExisting,
          HttpStatus.OK,
          'Find todo successfully!',
        );
      }
      return CommonResult<null>(
        null,
        HttpStatus.NOT_FOUND,
        `Not find todo by id : ${id}`,
      );
    } catch (error) {
      return CommonResult<null>(
        null,
        HttpStatus.INTERNAL_SERVER_ERROR,
        'Oops! Something went wrong!',
        error,
      );
    }
  }

  async update(updateTodoDto: UpdateTodoDto): Promise<BaseResponse<Todos>> {
    try {
      const todoExisting = await this.findOne(updateTodoDto.id);
      if (todoExisting.statusCode === 200) {
        todoExisting.data.content = updateTodoDto.content;
        todoExisting.data.status = updateTodoDto.status;

        const todoEdited = await todoExisting.data.save();
        return CommonResult<Todos>(
          todoEdited,
          HttpStatus.OK,
          'Updated todo successfully!',
        );
      }
      return CommonResult<null>(
        null,
        HttpStatus.NOT_FOUND,
        `Not find todo by id : ${updateTodoDto.id}`,
      );
    } catch (error) {
      return CommonResult<null>(
        null,
        HttpStatus.INTERNAL_SERVER_ERROR,
        'Oops! Something went wrong!',
        error,
      );
    }
  }
}
