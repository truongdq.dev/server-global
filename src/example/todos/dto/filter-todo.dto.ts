import { ApiProperty } from '@nestjs/swagger';
import { IsEnum } from 'class-validator';
import { TodoStatus } from '../types';

export class FilterTodoDto {
  @ApiProperty({
    enumName: 'TodoStatus',
    enum: TodoStatus,
  })
  @IsEnum(TodoStatus)
  status: boolean;
}
