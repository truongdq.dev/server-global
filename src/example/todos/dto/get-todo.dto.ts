import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNumber, IsString } from 'class-validator';
import { TodoStatus } from '../types';

export class GetTodoDto {
  @ApiProperty({
    type: Number,
  })
  @IsNumber()
  public id: number;

  @ApiProperty({
    type: String,
  })
  @IsString()
  public content: string;

  @ApiProperty({
    type: Boolean,
    enum: TodoStatus,
  })
  @IsEnum(TodoStatus)
  status: boolean;
}
