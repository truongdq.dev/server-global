import { ApiProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';
import { CreateTodoDto } from './create-todo.dto';

// PartialType() is function return a type (class) with all properties of input  type and set each child optional.
export class UpdateTodoDto extends CreateTodoDto {
  @ApiProperty({
    type: Number,
  })
  @IsNumber()
  id: number;
}

// PickType() is function construct a new type (class) by picking a set properties from an input type
/**
 * example
 * input type has : [name : required ,age : required] => using PickType [name] => [name : required ,age : optional]
 *
 */

//The OmitType() function constructs a type by picking all properties from an input type and then removing a particular set of keys.
/**
 * example
 * input type has : [name : required ,age : required] => using OmitType [name] => [age : required]
 *
 */

//e IntersectionType() function combines two types into one new type (class).
/**
 * example
 * input type has : 1 : [name : required ,age : required], 2: [color : required] => using IntersectionType [1,2] => [name : required , age : required, color : required]
 *
 */

//The type mapping utility functions are composable.
