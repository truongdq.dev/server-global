import { ApiProperty } from '@nestjs/swagger';
import { BaseDtoResponse } from '@base/types';
import { GetTodoDto } from './get-todo.dto';

export class ResponseTodoDto extends BaseDtoResponse {
  @ApiProperty({
    type: GetTodoDto,
  })
  public data: GetTodoDto;
}

export class ResponseListTodoDto extends BaseDtoResponse {
  @ApiProperty({
    type: [GetTodoDto],
  })
  public data: GetTodoDto[];
}
