import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsString } from 'class-validator';
import { TodoStatus } from '../types';

export class CreateTodoDto {
  @ApiProperty({
    type: String,
  })
  @IsString()
  content: string;

  @ApiProperty({
    type: String,
    enum: TodoStatus,
  })
  @IsEnum(TodoStatus)
  // @NotEquals(TodoStatus[TodoStatus.NOT_INIT])
  status: boolean;
}
