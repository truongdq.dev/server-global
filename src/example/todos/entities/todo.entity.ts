import { IsString } from 'class-validator';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { BuiltInBaseEntity } from '@base/entity';
import { TodoStatus } from '../types';

@Entity()
export class Todos extends BuiltInBaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @IsString()
  @Column({ nullable: false })
  content: string;

  @Column({ type: 'enum', enum: TodoStatus, default: TodoStatus.NOT_INIT })
  status: boolean;
}
