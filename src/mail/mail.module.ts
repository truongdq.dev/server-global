import { ConfigService } from '@nestjs/config';
import { MailerModule } from '@nestjs-modules/mailer';
import { Global, Module } from '@nestjs/common';
import { MailService } from './mail.service';
import { join } from 'path';
import { PugAdapter } from '@nestjs-modules/mailer/dist/adapters/pug.adapter';
const mailer__server__global__service = MailerModule.forRootAsync({
  useFactory: async (configService: ConfigService) => ({
    transport: {
      host: configService.get<string>('MAILER_HOST'),
      secure: false,
      auth: {
        user: configService.get<string>('MAILER_USER'),
        pass: configService.get<string>('MAILER_PASS'),
      },
    },
    defaults: {
      from: '"No Reply" <noreply@example.com>',
    },
    template: {
      dir: join(__dirname, 'templates'),
      adapter: new PugAdapter(),
      options: {
        strict: true,
      },
    },
  }),
  inject: [ConfigService],
});

@Global()
@Module({
  imports: [mailer__server__global__service],

  providers: [MailService],
  exports: [MailService, MailerModule],
})
export class MailModule {}
