import { AuthService } from './auth/auth.service';
import { Controller, Get } from '@nestjs/common';
import { AppService, Movie } from './app.service';
import { ApiTags } from '@nestjs/swagger';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private authService: AuthService,
  ) {}

  @ApiTags('Example')
  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @ApiTags('Example')
  @Get('/movies')
  getMovies(): Movie[] {
    return this.appService.getMovies();
  }
}
