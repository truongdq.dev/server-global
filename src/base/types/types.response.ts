import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';

export interface BaseResponse<T> {
  data?: T | null;
  errors?: any | null;
  statusCode: number;
  message?: string;
}

export class BaseDtoResponse {
  @ApiProperty({})
  public errors?: any;

  @ApiProperty({
    type: Number,
  })
  @IsNumber()
  public statusCode: number;

  @ApiProperty({
    type: String,
  })
  @IsString()
  public message?: string;
}
