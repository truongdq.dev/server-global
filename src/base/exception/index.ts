import { BaseResponse } from '@base/types';
import { HttpStatus } from '@nestjs/common';

export class BuiltInHttpException<T> {
  private readonly data;
  private readonly statusCode;
  private readonly message;
  private readonly errors;

  constructor(data: T, statusCode: HttpStatus, message: string, errors?: any) {
    this.data = data;
    this.statusCode = statusCode;
    this.message = message;
    this.errors = errors;
  }
}

export function CommonResult<T>(
  data: T,
  statusCode: HttpStatus,
  message: string,
  errors?: any,
): BaseResponse<T> {
  return {
    data,
    statusCode,
    message,
    errors,
  };
}
