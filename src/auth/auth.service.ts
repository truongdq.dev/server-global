import { UpdateUserDto } from './../users/dto/update-user.dto';
import { UsersService } from '~/src/users/users.service';
import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Users } from '~/src/users/entities/users.entity';
import { CreateUserDto } from '~/src/users/dto/create-user.dto';
import * as bcrypt from 'bcrypt';
import { MailService } from '../mail/mail.service';
import { IInputForgot } from './types';
import { ConfigService } from '@nestjs/config';
import { CommonResult } from '../base/exception';
import { GetUserDto, ResponseUserDto } from '../users/dto/get-user.dto';
import {
  FieldChangePasswordResponseDto,
  UserChangePasswordDto,
  UserChangePasswordResponseDto,
} from './dto/user-change-password.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private mailService: MailService,
    private configService: ConfigService,
  ) {}
  private logger = new Logger(AuthService.name);
  private saltOrRounds = 10;

  async validateUser(username: string, password: string): Promise<any> {
    const user = await this.usersService.findOne(username);
    if (!user) return null;
    const isMatch = await bcrypt.compare(password, user.password);
    if (isMatch) {
      return { ...user, password: password };
    }
    return null;
  }

  async findUser(username: string): Promise<Users | null> {
    const user = await this.usersService.findOne(username);
    if (!user) return null;
    return user;
  }

  async login(user: Users): Promise<{ access_token: string }> {
    if (!user.token || (user.token && !this.verifyToken(user.token))) {
      const access_token = await this.jwtService.sign({
        username: user.username,
        password: user.password,
        email: user.email,
      });

      await this.usersService.update({
        id: user.id,
        token: access_token,
        updated_at: new Date(),
      });
      return {
        access_token,
      };
    }
    return {
      access_token: user.token,
    };
  }

  async register(createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  async getMe(username: string): Promise<ResponseUserDto> {
    try {
      const user = await this.usersService.findOne(username);

      if (!user) {
        return CommonResult<null>(
          null,
          HttpStatus.NOT_FOUND,
          'User or token failure!',
        );
      }
      if (!user.token) {
        return CommonResult<null>(
          null,
          HttpStatus.FORBIDDEN,
          'Token expiresIn, Please login again!',
        );
      }
      const { password: _p, token: _t, ...rest } = user;
      return CommonResult<GetUserDto>(
        rest,
        HttpStatus.OK,
        'Profile successfully!',
      );
    } catch (error) {
      return CommonResult<null>(
        null,
        HttpStatus.INTERNAL_SERVER_ERROR,
        'Oops! Something went wrong!',
        error,
      );
    }
  }

  verifyToken(token: string): boolean {
    try {
      const decoded = this.jwtService.verify(token, {
        secret: this.configService.get<string>('JWT_SECRET'),
      });

      if (decoded) {
        return true;
      }
      return false;
    } catch (_error) {
      return false;
    }
  }

  async changePassword(
    jwtInfo: any,
    userChangePasswordDto: UserChangePasswordDto,
  ): Promise<UserChangePasswordResponseDto> {
    try {
      const userExisting = await this.usersService.findOne(jwtInfo.username);

      // check user existing!
      if (!userExisting) {
        return CommonResult<null>(
          null,
          HttpStatus.NOT_FOUND,
          'User not found!',
        );
      }

      // check compare password

      const isMatch = await bcrypt.compare(
        userChangePasswordDto.oldPassword,
        userExisting.password,
      );
      if (!isMatch) {
        return CommonResult<FieldChangePasswordResponseDto>(
          { status: false },
          HttpStatus.BAD_REQUEST,
          'Old password incorrect!',
        );
      }

      const hashPassword = await bcrypt.hash(
        userChangePasswordDto.newPassword,
        this.saltOrRounds,
      );

      await this.usersService.update({
        id: userExisting.id,
        password: hashPassword,
        updated_at: new Date(),
      });

      return CommonResult<FieldChangePasswordResponseDto>(
        { status: true },
        HttpStatus.OK,
        'Password change successfully!',
      );
    } catch (error) {
      return CommonResult<FieldChangePasswordResponseDto>(
        { status: false },
        HttpStatus.INTERNAL_SERVER_ERROR,
        'Oops! Something went wrong!',
        error,
      );
    }
  }

  async updateProfile(updateUserDto: UpdateUserDto): Promise<ResponseUserDto> {
    try {
      const userUpdated = await this.usersService.update(updateUserDto);
      const { password: _p, token: _t, ...rest } = userUpdated;
      return CommonResult<GetUserDto>(
        rest,
        HttpStatus.OK,
        'Update profile successfully!',
      );
    } catch (error) {
      return CommonResult<GetUserDto>(
        null,
        HttpStatus.INTERNAL_SERVER_ERROR,
        'Oops! Something went wrong!',
      );
    }
  }
}
