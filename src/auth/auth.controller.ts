import { ResponseUserDto } from './../users/dto/get-user.dto';
import {
  Body,
  Controller,
  Get,
  Patch,
  Post,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiProperty,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { AuthService } from './auth.service';
import { UserResponseLoginDto, UserLoginDto } from './dto/user-login.dto';
import { JwtAuthGuard } from './jwt-auth.guard';
import { LocalAuthGuard } from './local-auth.guard';
import {
  UserChangePasswordDto,
  UserChangePasswordResponseDto,
} from './dto/user-change-password.dto';
import { UpdateUserDto } from '../users/dto/update-user.dto';

@Controller({
  path: 'auth',
  version: '1',
})
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('register')
  @ApiTags('auth')
  @ApiProperty({ type: CreateUserDto })
  @ApiResponse({
    type: ResponseUserDto,
  })
  async register(@Body() createUserDto: CreateUserDto) {
    return this.authService.register(createUserDto);
  }

  @Post('login')
  @UseGuards(LocalAuthGuard)
  @ApiTags('auth')
  @ApiProperty({ type: UserLoginDto })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden.' })
  @ApiCreatedResponse({
    description: 'Login success!',
    type: UserResponseLoginDto,
  })
  async login(@Req() req, @Body() userLoginDto: UserLoginDto) {
    return this.authService.login(req.user);
  }

  @Get('me')
  @ApiTags('auth')
  @ApiResponse({
    type: ResponseUserDto,
  })
  @UseGuards(JwtAuthGuard)
  async me(@Req() req) {
    return this.authService.getMe(req.user.username);
  }

  @ApiTags('auth')
  @ApiProperty({ type: UserChangePasswordDto })
  @ApiResponse({
    type: UserChangePasswordResponseDto,
  })
  @Patch('change-password')
  @UseGuards(JwtAuthGuard)
  async changePassword(
    @Req() req,
    @Body() userChangePasswordDto: UserChangePasswordDto,
  ) {
    return this.authService.changePassword(req.user, userChangePasswordDto);
  }

  @ApiTags('auth')
  @ApiProperty({ type: UpdateUserDto })
  @ApiResponse({
    type: ResponseUserDto,
  })
  @Put('update-profile')
  @UseGuards(JwtAuthGuard)
  async updateProfile(@Body() updateUserDto: UpdateUserDto) {
    return this.authService.updateProfile(updateUserDto);
  }
}
