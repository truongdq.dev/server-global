import { ApiProperty } from '@nestjs/swagger';
import { IsString, MinLength } from 'class-validator';
import { BaseDtoResponse } from '@base/types';

export class UserChangePasswordDto {
  @ApiProperty({
    minLength: 8,
    required: true,
    type: String,
    example: '**********',
  })
  @IsString()
  @MinLength(5)
  public oldPassword: string;

  @ApiProperty({
    minLength: 8,
    required: true,
    type: String,
    example: '**********',
  })
  @IsString()
  @MinLength(8)
  public newPassword: string;
}

export class FieldChangePasswordResponseDto {
  @ApiProperty({
    type: Boolean,
    description:
      'Response true is change password successfully and and vice versa!',
  })
  public status: boolean;
}

export class UserChangePasswordResponseDto extends BaseDtoResponse {
  @ApiProperty({
    type: FieldChangePasswordResponseDto,
  })
  public data?: FieldChangePasswordResponseDto;
}
