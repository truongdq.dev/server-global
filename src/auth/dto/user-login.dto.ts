import { ApiProperty } from '@nestjs/swagger';
import { IsString, MinLength } from 'class-validator';

export class UserLoginDto {
  @ApiProperty({
    minLength: 5,
    required: true,
    type: String,
    example: 'john shoe',
  })
  @IsString()
  @MinLength(5)
  username: string;

  @ApiProperty({
    minLength: 8,
    required: true,
    type: String,
    example: '**********',
  })
  @IsString()
  @MinLength(8)
  password: string;
}

export class UserResponseLoginDto {
  @ApiProperty()
  access_token: string;
}
