import { MailModule } from './../mail/mail.module';
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { UsersModule } from '../users/users.module';
import { AuthService } from './auth.service';
import { LocalStrategy } from './local.strategy';
import { AuthController } from './auth.controller';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { JwtStrategy } from './jwt.strategy';

const passport__server__global__service = PassportModule.registerAsync({
  useFactory: async () => ({
    defaultStrategy: 'bearer',
  }),
  inject: [ConfigService],
});

const jwt__server__global_service = JwtModule.registerAsync({
  useFactory: async (configService: ConfigService) => ({
    secret: configService.get<string>('JWT_SECRET'),
    signOptions: {
      expiresIn: configService.get<string>('JWT_EXPIRE_IN'),
    },
  }),
  inject: [ConfigService],
});

@Module({
  imports: [
    passport__server__global__service,
    jwt__server__global_service,
    UsersModule,
    MailModule,
  ],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [AuthService, UsersModule, JwtModule],
  controllers: [AuthController],
})
export class AuthModule {}
