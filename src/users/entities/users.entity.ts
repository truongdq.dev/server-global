import {
  IsEmail,
  IsNumber,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { BuiltInBaseEntity } from '@base/entity';

@Entity()
export class Users extends BuiltInBaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  username: string;

  @Column({ unique: true })
  @IsEmail()
  email: string;

  @Column({ unique: true, nullable: true, default: () => 'NULL' })
  @IsString()
  @MinLength(10)
  @MaxLength(12)
  phone?: string | null;

  @Column({ type: 'longtext' })
  @IsString()
  @MinLength(8)
  password: string;

  @Column({
    type: 'longtext',
    nullable: true,
  })
  @IsString()
  token?: string | null;

  @Column({ default: true })
  status: boolean;

  @Column({ nullable: true, default: () => 'NULL' })
  avatar_url?: string | null;
}
