import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsNumber,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { CreateUserDto } from './create-user.dto';

export class UpdateUserDto extends PartialType(CreateUserDto) {
  @ApiProperty({
    type: Number,
  })
  @IsNumber()
  public id: number;

  @ApiProperty({
    type: Number,
  })
  @IsString()
  @MinLength(10)
  @MaxLength(12)
  public phone?: string;

  @ApiProperty({
    type: String,
  })
  @IsString()
  @MinLength(8)
  public password?: string;

  @ApiProperty({
    type: String,
  })
  public token?: string;

  @ApiProperty({
    type: Boolean,
  })
  public status?: boolean;

  @ApiProperty({
    type: String,
  })
  @IsString()
  public avatar_url?: string;

  @ApiProperty({
    type: Date,
  })
  public updated_at?: Date;
}
