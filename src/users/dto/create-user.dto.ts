import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsString, MinLength } from 'class-validator';

export class CreateUserDto {
  @ApiProperty({
    minLength: 5,
    required: true,
    type: String,
    example: 'john shoe',
  })
  @IsString()
  @MinLength(5)
  public username: string;

  @ApiProperty({
    required: true,
    type: String,
    example: 'johnshoe@gmail.com',
  })
  @IsString()
  @IsEmail()
  public email: string;

  @ApiProperty({
    minLength: 8,
    required: true,
    type: String,
    example: '**********',
  })
  @IsString()
  @MinLength(8)
  public password: string;
}
