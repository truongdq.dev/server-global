import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDate,
  IsEmail,
  IsNumber,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { BaseDtoResponse } from '@base/types';

export class GetUserDto {
  @ApiProperty({
    type: Number,
  })
  @IsNumber()
  public id: number;

  @ApiProperty({
    type: String,
  })
  @IsString()
  @MinLength(5)
  public username: string;

  @ApiProperty({
    type: String,
  })
  @IsString()
  @IsEmail()
  public email: string;

  @ApiProperty({
    type: Number,
  })
  @IsString()
  @MinLength(10)
  @MaxLength(12)
  public phone?: string;

  @ApiProperty({
    type: Boolean,
  })
  @IsBoolean()
  public status: boolean;

  @ApiProperty({
    type: String,
  })
  @IsString()
  public avatar_url?: string;

  @ApiProperty({
    type: Date,
  })
  @IsDate()
  public created_at?: Date;

  @ApiProperty({
    type: Date,
  })
  @IsDate()
  public updated_at?: Date;
}

export class ResponseUserDto extends BaseDtoResponse {
  @ApiProperty({
    type: GetUserDto,
  })
  public data?: GetUserDto;
}
