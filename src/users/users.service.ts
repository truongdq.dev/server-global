import { CommonResult } from '@base/exception';
import { UpdateUserDto } from './dto/update-user.dto';
import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { Users } from './entities/users.entity';
import * as bcrypt from 'bcrypt';
import { GetUserDto, ResponseUserDto } from './dto/get-user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Users) private userRepository: Repository<Users>,
  ) {}
  private logger = new Logger(UsersService.name);
  private saltOrRounds = 10;
  async create(createUserDto: CreateUserDto): Promise<ResponseUserDto> {
    try {
      const { username, password, email } = createUserDto;

      const checkUserExisting = await this.userRepository.findOne({
        where: [{ username: username }, { email: email }],
      });
      if (checkUserExisting) {
        const errors = [];
        if (username === checkUserExisting.username) {
          errors.push({
            field: 'username',
            message: 'Username already exists',
          });
        }
        if (email === checkUserExisting.email) {
          errors.push({
            field: 'email',
            message: 'Email already exists',
          });
        }
        return CommonResult<null>(
          null,
          HttpStatus.BAD_REQUEST,
          'Oops! Duplicate user info!',
          errors,
        );
      }

      const hashPassword = await bcrypt.hash(password, this.saltOrRounds);

      const newUser = this.userRepository.create({
        username,
        password: hashPassword,
        email,
      });
      await newUser.save();
      const { password: _p, token: _t, ...rest } = newUser;

      return CommonResult<GetUserDto>(
        rest,
        HttpStatus.OK,
        'Register successfully!',
      );
    } catch (error) {
      return CommonResult<null>(
        null,
        HttpStatus.INTERNAL_SERVER_ERROR,
        'Oops! Something went wrong!',
        error,
      );
    }
  }

  async findOne(usernameOrEmail: string): Promise<Users | null> {
    const user = await this.userRepository.findOne({
      where: [{ username: usernameOrEmail }, { email: usernameOrEmail }],
    });
    if (!user) return null;
    return user;
  }

  async update(updateUserDto: UpdateUserDto) {
    const {
      username,
      password,
      token = null,
      avatar_url,
      phone,
      email,
      status,
      id,
    } = updateUserDto;

    const userExisting = await this.userRepository.findOne({
      where: { id: id },
    });
    if (!userExisting) return null;
    if (username) userExisting.username = username;
    if (email) userExisting.email = email;
    if (phone) userExisting.phone = phone;
    userExisting.token = token;
    if (status) userExisting.status = status;
    if (avatar_url) userExisting.avatar_url = avatar_url;
    if (password) userExisting.password = password;
    userExisting.updated_at = new Date();

    await this.userRepository.save(userExisting);
    return userExisting;
  }
}
