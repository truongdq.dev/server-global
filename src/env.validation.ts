import { plainToClass } from 'class-transformer';
import { IsEnum, IsNumber, IsString, validateSync } from 'class-validator';

enum Environment {
  development = 'development',
  production = 'production',
  staging = 'staging',
}

class EnvironmentVariables {
  @IsEnum(Environment)
  NODE_ENV: Environment;

  @IsNumber()
  PORT: number;

  @IsNumber()
  CACHE_TTL: number;

  @IsNumber()
  HTTP_TIMEOUT: number;

  @IsNumber()
  MAX_REDIRECTS: number;

  @IsString()
  MONGO_URL: string;

  @IsString()
  JWT_SECRET: string;

  @IsString()
  JWT_EXPIRE_IN: string;

  @IsString()
  CORS_ALLOW_ORIGINS: string;

  @IsString()
  DB_MYSQL_HOST: string;

  @IsNumber()
  DB_MYSQL_PORT: number;

  @IsString()
  DB_MYSQL_USERNAME: string;

  @IsString()
  DB_MYSQL_PASSWORD: string;

  @IsString()
  DB_MYSQL_DB_NAME: string;
}

export function validate(config: Record<string, unknown>) {
  const validatedConfig = plainToClass(EnvironmentVariables, config, {
    enableImplicitConversion: true,
  });
  const errors = validateSync(validatedConfig, {
    skipMissingProperties: false,
  });

  if (errors.length > 0) {
    throw new Error(errors.toString());
  }
  return validatedConfig;
}
