import { AuthService } from './auth/auth.service';
import { CacheInterceptor, CacheModule, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { validate } from './env.validation';
import { TodosModule } from './example/todos/todos.module';
import { AuthModule } from './auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MailModule } from './mail/mail.module';
import { NotificationsModule } from './notifications/notifications.module';
import { BuiltValidationPipe } from './base/validator/validation.pipe';

const mongo__server__global__service = MongooseModule.forRootAsync({
  useFactory: async (configService: ConfigService) => ({
    uri: configService.get<string>('MONGO_URL'),
  }),
  inject: [ConfigService],
});

const cache__server__global__service = CacheModule.registerAsync({
  useFactory: async (configService: ConfigService) => ({
    ttl: configService.get<number>('CACHE_TTL'),
    isGlobal: true,
  }),
  inject: [ConfigService],
});

const sql__server__global__service = TypeOrmModule.forRootAsync({
  useFactory: async (configService: ConfigService) => ({
    type: 'mysql',
    host: configService.get<string>('DB_MYSQL_HOST'),
    port: configService.get<number>('DB_MYSQL_PORT'),
    username: configService.get<string>('DB_MYSQL_USERNAME'),
    password: configService.get<string>('DB_MYSQL_PASSWORD'),
    database: configService.get<string>('DB_MYSQL_DB_NAME'),
    entities: [__dirname + '/**/*.entity{.ts,.js}'],
    synchronize: true,
    autoLoadEntities: true,
    keepConnectionAlive: true,
  }),
  inject: [ConfigService],
});

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, cache: true, validate: validate }),
    cache__server__global__service,
    mongo__server__global__service,
    sql__server__global__service,
    ScheduleModule.forRoot(),
    TodosModule,
    AuthModule,
    MailModule,
    NotificationsModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    AuthService,
    // {
    //   provide: APP_INTERCEPTOR,
    //   useClass: CacheInterceptor,
    // },
    {
      provide: APP_PIPE,
      useClass: BuiltValidationPipe,
    },
  ],
})
export class AppModule {}
