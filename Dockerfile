# syntax=docker/dockerfile:1
FROM node:12.19-alpine3.9 AS development

WORKDIR /app

COPY package*.json yarn.lock ./

RUN yarn install --only=development

COPY . .

RUN yarn run build

FROM node:12.19.0-alpine3.9 AS production

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /app

COPY package*.json yarn.lock ./

RUN yarn install --only=production

COPY . .

COPY --from=development /app/dist ./dist

CMD ["node", "dist/main"]